from django.urls import path
from .views import (
    api_list_services,
    api_list_technicians,
    api_show_services,
    api_show_technician,
    api_list_filter,
    api_show_filter)

urlpatterns = [
    path("services/<int:auto_vo_id>/services/",
         api_list_services, name="api_list_services"),
    path("services/", api_list_services, name="api_create_services"),
    path("services/<int:pk>/", api_show_services, name="show_services"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_show_technician,
         name="api_show_technicians"),
    path("services/finished/", api_list_filter, name="api_list_filter"),
    path("services/<int:pk>/finished/", api_show_filter, name="api_show_filter"),
]
