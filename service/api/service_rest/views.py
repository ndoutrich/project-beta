from .models import TechnicianModel, ServiceAppointment
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import (
    TechnicianDetailEncoder,
    TechnicianListEncoder,
    ServiceDetailEncoder,
    ServiceListEncoder)


@require_http_methods(["GET"])
def api_list_filter(request):
    if request.method == "GET":
        services = ServiceAppointment.objects.filter(finished=False)
        return JsonResponse({"services": services}, encoder=ServiceListEncoder)


@require_http_methods(["PUT"])
def api_show_filter(request, pk):
    content = json.loads(request.body)
    service = ServiceAppointment.objects.get(id=pk).update(**content)
    return JsonResponse(
        service,
        encoder=ServiceDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_services(request):
    if request.method == "GET":
            services = ServiceAppointment.objects.all()
            return JsonResponse(
            {"services": services},
            encoder=ServiceListEncoder,
        )

    else:
        content = json.loads(request.body)
        technician_id = content["technician"]
        technician = TechnicianModel.objects.get(id=technician_id)
        content["technician"] = technician
        service = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            service,
            encoder=ServiceDetailEncoder,
            safe=False
        )


require_http_methods(["DELETE", "GET", "PUT"])


def api_show_services(request, pk):
    if request.method == "GET":
        service = ServiceAppointment.objects.get(pk=pk)
        return JsonResponse(
            service,
            encoder=ServiceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        ServiceAppointment.objects.filter(id=pk).update(**content)
        service = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            service,
            encoder=ServiceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = TechnicianModel.objects.all()
        return JsonResponse(
            {"technicians": technician},
            encoder=TechnicianListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = TechnicianModel.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False
        )


@require_http_methods(["Delete", "GET", "PUT"])
def api_show_technician(request, pk):
    if request.method == "GET":
        technician = TechnicianModel.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = TechnicianModel.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        TechnicianModel.objects.filter(id=pk).update(**content)
        technician = TechnicianModel.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
