import React from "react";

class ModelForm extends React.Component {
    state = {
        name: '',
        picture_url: '',
        manufacturers: [],
        manufacturer_id: '',
    };

    handleChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ... this.state };
        delete data.manufacturers;

        const modelUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: "",
                picture_url: "",
                manufacturer_id: "",
            };
            this.setState(cleared);
        };
    };

    componentDidMount = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers });
        };
    };

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a vehicle model</h1>
                        <form onSubmit={this.handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.picture_url} onChange={this.handleChange} placeholder="Picture url" required type="link" name="picture_url" id="picture_url"
                                    className="form-control" />
                                <label htmlFor="name">Picture url</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.manufacturer_id} onChange={this.handleChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                                    <option value="">Choose a manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    };
};

export default ModelForm;
