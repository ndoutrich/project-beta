import React, { useEffect, useState } from 'react';

export default function ServiceHistory() {

    const [appointments, setAppointments] = useState([]);
    const [filteredResults, setFilteredResults] = useState([]);
    const [searchInput, setSearchInput] = useState("");

    const fetchData = async () => {
        let data = await fetch("http://localhost:8080/api/services/");
        let response = await data.json();
        let appointmentsData = response.services
        setAppointments(appointmentsData);
        setFilteredResults(appointmentsData)
    }

    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        if (searchInput) {
            let filteredData = appointments.filter(appointment => appointment.vin.includes(searchInput))
            setFilteredResults(filteredData)
        } else {
            setFilteredResults(appointments)
        }
    }, [searchInput])

    return (
        <>
            <header className="mt-2 mb-2">
                <h1>Service history</h1>
            </header>
            <div style={{ padding: 20 }}>
                <input icon='search'
                    placeholder='Search by VIN'
                    onChange={(e) => setSearchInput(e.target.value)}
                    type="text">
                </input>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {searchInput.length >= 1 ? (
                        filteredResults.map((appointment) => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer_name}</td>
                                    <td>{appointment.date} </td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.name}</td>
                                    <td>{appointment.reason}</td>
                                </tr>
                            )
                        })
                    ) : (
                        appointments.map((appointment) => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer_name}</td>
                                    <td>{appointment.date} </td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.name}</td>
                                    <td>{appointment.reason}</td>
                                </tr>
                            )
                        })
                    )}
                </tbody>
            </table>
        </>
    )
}
