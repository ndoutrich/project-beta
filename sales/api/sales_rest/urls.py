from django.urls import path

from .views import (
    api_list_customers,
    api_list_sales_persons,
    api_list_sales_records,
    api_show_customer,
    api_show_sales_person,
    api_show_sales_record
    )

urlpatterns = [
    path("automobiles/<int:automobile_vo_id>/sales_records/", api_list_sales_records, name="api_list_sales_records"),
    path("sales_records/", api_list_sales_records, name="api_create_sales_records"),
    path("sales_records/<int:pk>/", api_show_sales_record, name="api_show_sales_record"),
    path("customers/", api_list_customers, name="api_create_customers"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("sales_persons/", api_list_sales_persons, name="api_create_sales_persons"),
    path("sales_persons/<int:pk>/", api_show_sales_person, name="api_show_sales_person"),
    ]
