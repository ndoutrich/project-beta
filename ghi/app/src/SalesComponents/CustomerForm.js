import React from "react";

class CustomerForm extends React.Component {
    state = {
        name: "",
        address: "",
        phone_number: "",
    };

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ... this.state };
        const customersUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(customersUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: "",
                address: "",
                phone_number: "",
            };
            this.setState(cleared);
        };
    };

    handleChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    };

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Register a customer</h1>
                        <form onSubmit={this.handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.address} onChange={this.handleChange} placeholder="Address" required type="text" name="address" id="address"
                                    className="form-control" />
                                <label htmlFor="style_name">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.phone_number} onChange={this.handleChange} placeholder="Phone number" required type="text" pattern="[0-9]*"
                                    name="phone_number" id="phone_number"
                                    className="form-control" />
                                <label htmlFor="style_name">Phone number(ex. 0123456789)</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    };
};

export default CustomerForm;
