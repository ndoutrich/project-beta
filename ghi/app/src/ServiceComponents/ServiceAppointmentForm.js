import React from "react";

class ServiceAppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            autos: [],
            customerName: "",
            date: "",
            time: "",
            technician: "",
            technicians: [],
            reason: "",
            vin: "",
        };
        this.inputChange = this.inputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ... this.state };
        data.customer_name = data.customerName;
        delete data.customerName;
        delete data.technicians;

        for (let auto of this.state.autos) {
            if (auto.vin === data.vin) {
                data.vip = true;
            }
        }
        delete data.autos

        const locationUrl = "http://localhost:8080/api/services/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                customerName: "",
                date: "",
                time: "",
                technician: "",
                reason: "",
                vin: "",
            };
            this.setState(cleared);
        }
    }

    inputChange(event) {
        const value = event.target.value
        const name = event.target.name
        this.setState({ [name]: value })
    }

   async componentDidMount() {
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const autoResponse = await fetch(autoUrl);
        if (autoResponse.ok) {
            const autoData = await autoResponse.json();
            this.setState({autos: autoData.autos})
        }

        const techUrl = "http://localhost:8080/api/technicians/";
        const techResponse = await fetch(techUrl);
        if (techResponse.ok) {
            const techData = await techResponse.json();
            this.setState({ technicians: techData.technicians })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create an appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.vin} onChange={this.inputChange} placeholder="vin" required type="text" name="vin" id="vin"
                                    className="form-control" />
                                <label htmlFor="name">Vin</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.customerName} onChange={this.inputChange} placeholder="customerName" required type="text" name="customerName" id="customerName"
                                    className="form-control" />
                                <label htmlFor="style_name">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.date} onChange={this.inputChange} placeholder="Date" required type="date" name="date" id="date"
                                    className="form-control" />
                                <label htmlFor="style_name">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.time} onChange={this.inputChange} placeholder="Time" required type="time" name="time" id="time"
                                    className="form-control" />
                                <label htmlFor="style_name">Time</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.technician} onChange={this.inputChange} required name="technician" id="technician" className="form-select">
                                    <option value="">Choose a technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.tech_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.reason} onChange={this.inputChange} placeholder="Reason" required type="text" name="reason" id="reason"
                                    className="form-control" />
                                <label htmlFor="style_name">Reason</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ServiceAppointmentForm
