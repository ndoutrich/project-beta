import React from 'react';


class SalesList extends React.Component {
    state = {
        sales_records: [],
        isLoaded: false,
    };

    getSalesRecords = async () => {
        const response = await fetch('http://localhost:8090/api/sales_records');
        if (response.ok) {
            const data = await response.json();
            const sales_records = data.sales_records;
            this.setState({
                sales_records: sales_records,
                isLoaded: true,
            });
        };
    };


    componentDidMount() {
        this.getSalesRecords();
    }

    render() {
        return (
            <>
                <header className="mt-2 mb-2">
                    <h1>Sales records</h1>
                </header>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales person</th>
                            <th>Employee number</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales_records.map(salesRecord => {
                            return (
                                <tr key={salesRecord.id}>
                                    <td>{salesRecord.sales_person[0]}</td>
                                    <td>{salesRecord.sales_person[1]}</td>
                                    <td>{salesRecord.customer} </td>
                                    <td>{salesRecord.automobile}</td>
                                    <td>{salesRecord.price}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    };
};

export default SalesList;
