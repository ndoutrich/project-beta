from django.db import models
from django.urls import reverse


class AutoMobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class TechnicianModel(models.Model):
    tech_name = models.CharField(max_length=100)
    employee_number = models.PositiveBigIntegerField()

    def get_api_url(self):
        return reverse("show_tech", kwargs={"pk": self.pk})


class ServiceAppointment(models.Model):
    customer_name = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    reason = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)
    finished = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        TechnicianModel,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_services", kwargs={"pk": self.pk})
