from django.db import models
from django.urls import reverse
# Create your models here.


class AutomobileVO(models.Model):
    href = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_show_sales_people", kwargs={"pk": self.pk})


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_show_customers", kwargs={"pk": self.pk})


class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_persons",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.CASCADE,
    )
    price = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_show_sales_records", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("automobile", "id")
