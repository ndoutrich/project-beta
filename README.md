# CarCar

Team:

* Person 1 - Trung Nguyen (Service)
* nick doutrich - auto sales

## Design
Created two microservices that poll data from another microservice.
Those two microservices work under the monolith of inventory to
handle different departments of the auto company.

A poller was used to pull automobile VIN numbers to store within both micorservices.
As information about sales and services was updated or changed this would change
certain properties on automobiles from the inventory microservice as well.

This communication and interaction between microservices allows for a good
flow of data from each microservice and from the back to front of each application.


## Service microservice
Technician Model
Service Appointment Model
AutomobileVO
    - Uses Vo to obtain Vin

## Sales microservice

Models:
    SalesPerson:
        name:
        employee_number:
    Customer:
        name:
        address:
        phone_number:
    SaleRecord:
        automobile: foreign key AutomobileVO
        sales_persoon: foreign key SalesPerson
        customer: foreign key Customer
        price:
    AutomobileVO:
        vin_number:
