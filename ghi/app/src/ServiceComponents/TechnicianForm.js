import React from 'react'

class TechForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            techName: '',
            employeeNumber: '',
        };
        this.inputChange = this.inputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.employee_number = data.employeeNumber;
        data.tech_name = data.techName;
        delete data.employeeNumber
        delete data.techName;

        const locationUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                techName: "",
                employeeNumber: "",
            };
            this.setState(cleared);
        }
    }

    inputChange(event) {
        const value = event.target.value
        const name = event.target.name
        this.setState({ [name]: value })
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Register a technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.techName} onChange={this.inputChange} placeholder="techName" required type="text" name="techName" id="techName"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.employeeNumber} onChange={this.inputChange} placeholder="employeeNumber" required type="text" name="employeeNumber" id="employeeNumber"
                                    className="form-control" />
                                <label htmlFor="style_name">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default TechForm
