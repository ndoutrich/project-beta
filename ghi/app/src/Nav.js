import { NavLink, Link } from 'react-router-dom';
import NavDropdown from 'react-bootstrap/NavDropdown';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            </li>
            <li>
              <NavDropdown
                id="nav-dropdown"
                title="Inventory">
                <NavDropdown.Item to="manufacturers/" as={Link}>Manufacturers</NavDropdown.Item>
                <NavDropdown.Item to="models/" as={Link}>Vehicle models</NavDropdown.Item>
                <NavDropdown.Item to="automobiles/" as={Link}>Vehicle inventory</NavDropdown.Item>
                <NavDropdown.Item to="manufacturers/new" as={Link}>New manufacturer</NavDropdown.Item>
                <NavDropdown.Item to="models/new" as={Link}>New model</NavDropdown.Item>
                <NavDropdown.Item to="automobiles/new" as={Link}>New vehicle</NavDropdown.Item>
              </NavDropdown>
            </li>
            <li>
              <NavDropdown
                id="nav-dropdown"
                title="Sales">
                <NavDropdown.Item to="sales_records/" as={Link}>Sales records</NavDropdown.Item>
                <NavDropdown.Item to="employee_sales/" as={Link}>Employee sales</NavDropdown.Item>
                <NavDropdown.Item to="sales_records/new" as={Link}>New sales record</NavDropdown.Item>
                <NavDropdown.Item to="customer/new" as={Link}>New customer</NavDropdown.Item>
                <NavDropdown.Item to="sales_person/new" as={Link}>New sales person</NavDropdown.Item>
              </NavDropdown>
            </li>
            <li>
              <NavDropdown
                id="nav-dropdown"
                title="Services">
                <NavDropdown.Item to="services/" as={Link}>Appointments</NavDropdown.Item>
                <NavDropdown.Item to="service_history/" as={Link}>Service History</NavDropdown.Item>
                <NavDropdown.Item to="services/new" as={Link}>New appointment</NavDropdown.Item>
                <NavDropdown.Item to="technician/new" as={Link}>New technician</NavDropdown.Item>
              </NavDropdown>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
