import React from 'react';

class SalesRecordForm extends React.Component {
    state = {
        automobiles: [],
        automobile: "",
        salesPersons: [],
        sales_person: "",
        customers: [],
        customer: "",
        price: "",
    };

    handleChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state };
        delete data.automobiles;
        delete data.salesPersons;
        delete data.customers;

        const recordUrl = 'http://localhost:8090/api/sales_records/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const autoUrl = `http://localhost:8100/api/automobiles/${data.automobile}/`;
        const autoFetchConfig = {
            method: 'put',
            body: JSON.stringify({ sold: true }),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(recordUrl, fetchConfig);
        const response_auto = await fetch(autoUrl, autoFetchConfig);
        if (response.ok && response_auto.ok) {
            const cleared = {
                automobile: '',
                sales_person: '',
                customer: '',
                price: '',
            };
            this.setState(cleared);
            this.mountAutos();
        }


    }

    mountAutos = async () => {
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const autoResponse = await fetch(autoUrl);
        if (autoResponse.ok) {
            const autoData = await autoResponse.json();
            const filteredAutoData = autoData.autos.filter(auto => auto.sold === false);
            this.setState({ automobiles: filteredAutoData });
        }
    }

    componentDidMount = async () => {
        this.mountAutos();

        const salesPersonsUrl = 'http://localhost:8090/api/sales_persons/';
        const salesPersonsResponse = await fetch(salesPersonsUrl);
        if (salesPersonsResponse.ok) {
            const salesPersonsData = await salesPersonsResponse.json();
            this.setState({ salesPersons: salesPersonsData.sales_persons });
        }
        const customersUrl = 'http://localhost:8090/api/customers/';
        const customersResponse = await fetch(customersUrl);
        if (customersResponse.ok) {
            const customersData = await customersResponse.json();
            this.setState({ customers: customersData.customers });
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Record a new sale</h1>
                        <form onSubmit={this.handleSubmit} id="create-sales-record-form">
                            <div className="mb-3">
                                <select value={this.state.automobile} onChange={this.handleChange} required name="automobile" id="automobile" className="form-select">
                                    <option value="">Choose an automobile</option>
                                    {this.state.automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>
                                                {automobile.vin}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.sales_person} onChange={this.handleChange} required name="sales_person" id="sales_person" className="form-select">
                                    <option value="">Choose a sales person</option>
                                    {this.state.salesPersons.map(salesPerson => {
                                        return (
                                            <option key={salesPerson.id} value={salesPerson.id}>
                                                {salesPerson.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.customer} onChange={this.handleChange} required name="customer" id="customer" className="form-select">
                                    <option value="">Choose a customer</option>
                                    {this.state.customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>
                                                {customer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.price} onChange={this.handleChange} placeholder="Price" required type="number" name="price" id="price"
                                    className="form-control" />
                                <label htmlFor="model_name">Price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    };
};

export default SalesRecordForm;
