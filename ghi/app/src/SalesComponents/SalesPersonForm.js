import React from "react";

class SalesPersonForm extends React.Component {
    state = {
        name: "",
        employee_number: "",
    };

    handleChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ... this.state };
        const salesPersonsUrl = "http://localhost:8090/api/sales_persons/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesPersonsUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: "",
                employee_number: "",
            };
            this.setState(cleared);
        };
    };

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Register a sales person</h1>
                        <form onSubmit={this.handleSubmit} id="create-sales-person-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.employee_number} onChange={this.handleChange} placeholder="Employee number" required type="text" name="employee_number" id="employee_number"
                                    className="form-control" />
                                <label htmlFor="style_name">Employee number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    };
};

export default SalesPersonForm;
