import React from 'react';


class ModelsList extends React.Component {
    state = {
        models: []
    };

    getModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            const models = data.models;
            this.setState({
                models: models,
            });
        };
    };

    componentDidMount = () => {
        this.getModels();
    };

    render() {
        return (
            <>
                <header className="mt-2 mb-2">
                    <h1>Vehicle models</h1>
                </header>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td><img src={model.picture_url} /></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    };
};

export default ModelsList;
