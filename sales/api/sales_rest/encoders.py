from common.json import ModelEncoder
from .models import (
    AutomobileVO,
    SalesPerson,
    Customer,
    SalesRecord
)


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "id",
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
    ]


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
    ]


class SalesRecordListEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "id",
        "price",
        "sales_person"
    ]

    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "customer": o.customer.name,
            "sales_person": [o.sales_person.name, o.sales_person.employee_number]
        }


class SalesRecordDetailEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "price",
    ]
    encoders = {
        "sales_person": SalesPersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
        "automobile": AutomobileVODetailEncoder(),
    }
