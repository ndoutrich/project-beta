import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './SalesComponents/SalesPersonForm';
import SalesRecordForm from './SalesComponents/SalesRecordForm';
import SalesList from './SalesComponents/SalesList';
import ServiceAppointmentForm from './ServiceComponents/ServiceAppointmentForm';
import TechForm from './ServiceComponents/TechnicianForm';
import AppointmentList from './ServiceComponents/AppointmentList';
import SalesPersonSalesList from './SalesComponents/SalesPersonSales';
import ServiceHistory from './ServiceComponents/ServiceHistory';
import ManufacturersList from './InventoryComponents/ManufacturersList';
import ModelsList from './InventoryComponents/VehicleModelsList';
import AutomobileList from './InventoryComponents/AutomobilesList';
import ManufacturerForm from './InventoryComponents/ManufacaturersForm';
import ModelForm from './InventoryComponents/VehicleModelsForm';
import AutoForm from './InventoryComponents/AutomobilesForm';
import CustomerForm from './SalesComponents/CustomerForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="services/new" element={<ServiceAppointmentForm />} />
          <Route path="services/" element={<AppointmentList />} />
          <Route path="technician/new" element={<TechForm />} />
          <Route path="service_history/" element={<ServiceHistory />} />


          <Route path="employee_sales" element={<SalesPersonSalesList />} />
          <Route path="sales_person/new/" element={<SalesPersonForm />} />
          <Route path="customer/new/" element={<CustomerForm />} />
          <Route path="sales_records/new/" element={<SalesRecordForm />} />
          <Route path="sales_records/" element={<SalesList />} />

          <Route path="manufacturers/" element={<ManufacturersList />} />
          <Route path="models/" element={<ModelsList />} />
          <Route path="automobiles/" element={<AutomobileList />} />
          <Route path="manufacturers/new/" element={<ManufacturerForm />} />
          <Route path="models/new/" element={<ModelForm />} />
          <Route path="automobiles/new/" element={<AutoForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
