import React from 'react';


class AutomobileList extends React.Component {
    state = {
        autos: []
    };

    getAutos = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            const autos = data.autos;
            this.setState({
                autos: autos,
            });
        };
    };


    componentDidMount = () => {
        this.getAutos();
    };

    render() {
        return (
            <>
                <header className="mt-2 mb-2">
                    <h1>Vehicle inventory</h1>
                </header>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.autos.map(auto => {
                            return (
                                <tr key={auto.id}>
                                    <td>{auto.vin}</td>
                                    <td>{auto.color}</td>
                                    <td>{auto.year}</td>
                                    <td>{auto.model.name}</td>
                                    <td>{auto.model.manufacturer.name}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    };
};

export default AutomobileList;
