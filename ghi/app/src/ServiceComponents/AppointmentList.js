import React from 'react';


class AppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            services: [],
            isLoaded: false
        };
        this.finish = this.finish.bind(this);
    }

    async getServiceAppointments() {
        const response = await fetch('http://localhost:8080/api/services/finished/')
        if (response.ok) {
            const data = await response.json();
            const appointments = data.services;
            this.setState({
                services: appointments,
                isLoaded: true,
            });
        }
    }

    async finish(event) {
        const locationUrl = `http://localhost:8080/api/services/${event.id}/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({ finished: true }),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            this.getServiceAppointments();
        }
    }

    handleDelete = async (eventDelete) => {
        const deleteUrl = `http://localhost:8080/api/services/${eventDelete.id}/`
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(deleteUrl, fetchConfig);
        if (response.ok) {
            this.getServiceAppointments()
        }
    }

    componentDidMount = () => {
        this.getServiceAppointments()
    }

    isTrue = (value) => {
        if (value === true){
            return "YES"
            // return "✔"
        } else {
            return "NO"
            // return "x"
        }
    }
    render() {
        return (
            <>
                <header className="mt-2 mb-2">
                    <h1>Appointments</h1>
                </header>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer</th>
                            <th>VIP</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.services.map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer_name}</td>
                                    <td>{this.isTrue(appointment.vip)}</td>
                                    {/* <td>{String(appointment.vip)}</td> */}
                                    <td>{appointment.date} </td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button onClick={() => this.handleDelete(appointment)}>Cancel</button>
                                    </td>
                                    <td>
                                        <button onClick={() => this.finish(appointment)}>Finished</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    }
}

export default AppointmentList
