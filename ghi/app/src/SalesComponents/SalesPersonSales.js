import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';

class SalesList extends React.Component {
    state = {
        sales_person: "",
        customer: "",
        automobile: "",
        price: "",
        salesRecords: [],
        salesRecordsInd: [],
        salesPersons: [],
        isLoaded: false,
        dropdownTitle: "Choose an employee",
    };

    getSalesRecords = async () => {
        const response = await fetch('http://localhost:8090/api/sales_records');
        if (response.ok) {
            const data = await response.json();
            const salesRecords = data.sales_records;
            this.setState({
                salesRecords: salesRecords,
                isLoaded: true,
            });
        };
    };

    getSalesPersons = async () => {
        const response = await fetch('http://localhost:8090/api/sales_persons');
        if (response.ok) {
            const data = await response.json();
            const salesPersons = data.sales_persons;
            this.setState({
                salesPersons: salesPersons,
            });
        };
    };

    componentDidMount() {
        this.getSalesRecords();
        this.getSalesPersons();

    };

    handleChange = (salesPerson) => {
        const data = [];
        for (let salesRecord of this.state.salesRecords) {
            if (salesRecord.sales_person[1] === salesPerson.employee_number) {
                data.push(salesRecord);
            }
        }
        this.setState({
            salesRecordsInd: data,
            dropdownTitle: salesPerson.employee_number,
        });
    };

    render() {
        return (
            <>
                <header className="mt-2 mb-2">
                    <h1>Employee sales history</h1>
                </header>
                <DropdownButton className="btn-group d-inline-flex mt-2 mb-2" variant="success" menuVariant='dark' id="dropdown-menu" title={this.state.dropdownTitle}>
                    {this.state.salesPersons.map(salesPerson => {
                        return (
                            <Dropdown.Item onClick={() => this.handleChange(salesPerson)} key={salesPerson.employee_number}>{salesPerson.name} - {salesPerson.employee_number}</Dropdown.Item>
                        );
                    })}
                </DropdownButton>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.salesRecordsInd.map(salesRecord => {
                            return (
                                <tr key={salesRecord.id}>
                                    <td>{salesRecord.sales_person[0]}</td>
                                    <td>{salesRecord.customer} </td>
                                    <td>{salesRecord.automobile}</td>
                                    <td>{salesRecord.price}</td>
                                </tr>
                            )
                        }
                        )}
                    </tbody>
                </table>
            </>
        );
    };
};

export default SalesList;
