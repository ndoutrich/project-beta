from common.json import ModelEncoder
from service_rest.models import AutoMobileVO
from .models import AutoMobileVO, TechnicianModel, ServiceAppointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutoMobileVO
    properties = ["vin"]


class TechnicianDetailEncoder(ModelEncoder):
    model = TechnicianModel
    properties = ["tech_name", "employee_number"]


class TechnicianListEncoder(ModelEncoder):
    model = TechnicianModel
    properties = ["tech_name", "id"]


class ServiceListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "customer_name",
        "date",
        "reason",
        "id",
        "vin",
        "time",
        "finished",
        "vip",
    ]

    def get_extra_data(self, o):
        return {
            "name": o.technician.tech_name,
        }


class ServiceDetailEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "customer_name",
        "date",
        "reason",
        "finished",
        "vip",
    ]
    encoders = {
        "Auto_Vo": AutomobileVODetailEncoder(),
        "technician": TechnicianDetailEncoder(),
    }
