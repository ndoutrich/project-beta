import React from 'react';


class ManufacturersList extends React.Component {
    state = {
        manufacturers: [],
    };

    getManufacturers = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers')
        if (response.ok) {
            const data = await response.json();
            const manufacturers = data.manufacturers;
            this.setState({
                manufacturers: manufacturers,
            });
        };
    };

    componentDidMount = () => {
        this.getManufacturers();
    };

    render() {
        return (
            <>
                <header className="mt-2 mb-2">
                    <h1>Manufacturers</h1>
                </header>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.manufacturers.map(manufacturer => {
                            return (
                                <tr key={manufacturer.id}>
                                    <td>{manufacturer.name}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    };
};

export default ManufacturersList;
